# "ported" by Studio Revolucija <tech@revolucija.hr> from
#   https://github.com/CentOS/CentOS-Dockerfiles
#
# Originally written for CentOS-Dockerfiles by
#   https://github.com/maxamillion

FROM centos:centos7
MAINTAINER Studio Revolucija <tech@revolucija.hr>

RUN yum -y update && \
    yum -y install sudo epel-release && \
    rpm -Uvh http://yum.postgresql.org/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-1.noarch.rpm && \
    yum -y install postgresql94-server postgresql94 postgresql94-contrib supervisor pwgen && \
    yum clean all

ADD ./postgresql-setup /usr/pgsql-9.4/bin/postgresql-setup
ADD ./supervisord.conf /etc/supervisord.conf
ADD ./start_postgres.sh /start_postgres.sh

#Sudo requires a tty. fix that.
RUN sed -i 's/.*requiretty$/#Defaults requiretty/' /etc/sudoers && \
    chmod +x /usr/pgsql-9.4/bin/postgresql-setup && \
    chmod +x /start_postgres.sh

RUN /usr/pgsql-9.4/bin/postgresql-setup

ADD ./postgresql.conf /var/lib/pgsql/9.4/data/postgresql.conf
RUN chown -v postgres.postgres /var/lib/pgsql/9.4/data/postgresql.conf

RUN echo "local  all all peer" > /var/lib/pgsql/9.4/data/pg_hba.conf && \
    echo "host   all all 127.0.0.1/32 password" >> /var/lib/pgsql/9.4/data/pg_hba.conf && \
    echo "host   all all localhost password" >> /var/lib/pgsql/9.4/data/pg_hba.conf

VOLUME ["/var/lib/pgsql/9.4"]

EXPOSE 5432

CMD ["/bin/bash", "/start_postgres.sh"]
