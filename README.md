Studio Revolucija Dockerfiles
=============================

This repository provides Dockerfiles we use in combination with Bitbucket Pipelines to run automated tests.


## Which images to use?

Currently we have available plain PostgreSQL images without Django:

* centos7-postgresql9.2

* centos7-postgresql9.4

and PostgreSQL images with Django dependencies installed:

* centos7-postgresql9.2-django

* centos7-postgresql9.4-django

You should always use Django images when running tests via Bitbucket Pipelines. Depending on the project choose PostgreSQL 9.2 or 9.4.


## Building Images

We have included bp.sh script for batch building and pushing of Docker images. When running the scrip, provide postgres version as first argument, and build version as second argument:

    ./bp.sh 9.4 1.1


## Contribution Guidelines 

Each Dockerfile should contain a README that includes the following:

 * Instructions for running the Docker image

 * Instructions for building the Docker image
 
 * If yum is used during the build process, run a clean afterwards to reduce the image size.
