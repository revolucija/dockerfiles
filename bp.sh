#!/bin/bash

# build and push dockerfiles script which takes two arguments:
#   postgres_version that should be built: 9.2 or 9.4
#   version of the build, e.g. 1.2
# example usage:
# ./bp.sh 9.2 1.2
# ./bp.sh 9.4 1.1


postgres_version=$1
version=$2

if [[ -n "$version" ]] && [[ -n "$postgres_version" ]]; then
    docker build --rm -t revolucija/centos7-postgresql$postgres_version:$version centos7-postgresql$postgres_version/
    docker push revolucija/centos7-postgresql$postgres_version:$version

    docker build --rm -t revolucija/centos7-postgresql$postgres_version-django:$version centos7-postgresql$postgres_version-django/
    docker push revolucija/centos7-postgresql$postgres_version-django:$version
else
    echo "Please provide postgres version as first argument and build version as second argument. E.g. ./bpa.sh 9.4 1.2"
fi
