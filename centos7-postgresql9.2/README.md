CentOS7 and PostgreSQL 9.2 Dockerfile
=====================================

This repository contains a Dockerfile and some helper files used to build PostgreSQL 9.2 on CentOS 7.

Examples below are given for version 1.0 When building new images bump version numbers.


Running the Docker image
------------------------

You can create a postgresql superuser at launch by specifying `POSTGRESQL_USER` and `POSTGRESQL_PASSWORD` variables. You may also create a database by using `POSTGRESQL_DATABASE`.

    docker run -t -i --entrypoint=bash \
    -e 'POSTGRESQL_USER=pipelines_demo' \
    -e 'POSTGRESQL_PASSWORD=pipelines_demo' \
    -e 'POSTGRESQL_DATABASE=pipelines_demo' \
    --name=postgres92 \
    revolucija/centos7-postgresql9.2:1.0

After you run container with above run command, you get tty, however postgres is not started because we have overriden entry point. Run ./start_postgres.sh to start postgres. Now you can test if database and database user are created and whether you have all needed dependencies installed.


Create your own image
---------------------

If you want to customize this image, you can edit Dockerfile or any of the accompanying files and build the image with:

    docker build --rm -t <your-docker-account>/centos7-postgresql9.2:1.0 .

# NOTE: when changing this image, make sure you rebuild all dependant images, e.g. revolucija/centos7-postgresql9.2-django.


Push the image back to the Docker Hub
-------------------------------------

    docker push <your-docker-account>/centos7-postgresql9.2:1.0
