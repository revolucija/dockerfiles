#!/bin/bash

# database name, user and password can be passed as env variables
POSTGRESQL_DATABASE=${POSTGRESQL_DATABASE:-}
POSTGRESQL_USER=${POSTGRESQL_USER:-}
POSTGRESQL_PASSWORD=${POSTGRESQL_PASSWORD:-}
PG_CONFDIR="/var/lib/pgsql/data"

__create_user() {
  #Grant rights
  usermod -G wheel postgres

  # Check to see if we have pre-defined credentials to use
if [ -n "${POSTGRESQL_USER}" ]; then
  if [ -z "${POSTGRESQL_PASSWORD}" ]; then
    echo ""
    echo "WARNING: "
    echo "No password specified for \"${POSTGRESQL_USER}\". Generating one"
    echo ""
    POSTGRESQL_PASSWORD=$(pwgen -c -n -1 12)
    echo "Password for \"${POSTGRESQL_USER}\" created as: \"${POSTGRESQL_PASSWORD}\""
  fi
    echo "Creating user \"${POSTGRESQL_USER}\"..."
    sudo -u postgres psql -c "CREATE USER \"${POSTGRESQL_USER}\" with CREATEDB PASSWORD '${POSTGRESQL_PASSWORD}';"
fi

if [ -n "${POSTGRESQL_DATABASE}" ]; then
  echo "Creating database \"${POSTGRESQL_DATABASE}\"..."
  sudo -u postgres psql -c "CREATE DATABASE \"${POSTGRESQL_DATABASE}\" OWNER \"${POSTGRESQL_USER}\";"
fi
}


__run_supervisor() {
supervisord -c /etc/supervisord.conf
}

# Call all functions
__run_supervisor
# wait couple of seconds for postgres to start
sleep 5
__create_user
