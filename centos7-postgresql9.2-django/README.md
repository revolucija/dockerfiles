CentOS7, PostgreSQL 9.2 and Django Dockerfile
=============================================

This repository contains a Dockerfile used to build Django dependencies and PostgreSQL 9.2 on CentOS7.

Although image name contains Django, Django itself is not installed. However, most dependencies required do run Django projects are installed.

Examples below are given for version 1.0 When building new images bump version numbers.


Running the Docker image
------------------------

You can create a postgresql superuser at launch by specifying `POSTGRESQL_USER` and `POSTGRESQL_PASSWORD` variables. You may also create a database by using `POSTGRESQL_DATABASE`.

    docker run -t -i --entrypoint=bash \
    -e 'POSTGRESQL_USER=pipelines_demo' \
    -e 'POSTGRESQL_PASSWORD=pipelines_demo' \
    -e 'POSTGRESQL_DATABASE=pipelines_demo' \
    --name=django \
    revolucija/centos7-postgresql9.2-django:1.0

After you run container with above run command, you get tty, however postgres is not started because we have overriden entry point. Run ./start_postgres.sh to start postgres. Now you can test if database and database user are created and whether you have all needed dependencies installed.


Create your own image
---------------------

If you want to customize this image, you can edit Dockerfile and build the image with:

    docker build --rm -t <your-docker-account>/centos7-postgresql9.2-django:1.0 .

# NOTE: if revolucija/centos7-postgresql9.2 has changed, you have to rebuild it before building this image.


Push the image back to the Docker Hub
-------------------------------------

    docker push <your-docker-account>/centos7-postgresql9.2:1.0
